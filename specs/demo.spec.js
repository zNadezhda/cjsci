const chai = require('chai');

const { expect } = chai;

describe('Reverse String Test', () => {
  it('Checks if the strings is reversed', () => {
    const str1 = 'Mocha is cool!!';
    const str2 = 'Chai is super supportive.';

    expect(str1).to.equal('!!looc si ahcoM');
    expect(str2).to.equal('.evitroppus repus si iahC');
  });
});
